#include <stdio.h> // printf
#include <stdlib.h> // EXIT_SUCCESS, srand
#include <math.h> // M_PI, fabs

#include "pi_shared.h"

int main(int argc, char* argv[]) {
    int seed;
    int N;
    int hits;
    double my_pi;

    // get parameters from command line arguments
    read_arguments(&seed, &N, argc, argv);

    // generate N random coordinates, get number of hits
    // (see count_hits() function for details)
    hits = count_hits(seed, N);

    // estimate PI based on following assumptions
    //   area of circle                = PI * r^2   ~= hits
    //   area of square around circle  = (2 * r)^2  ~= N
    //   r = 1
    my_pi = 4. * ((double) hits  / (double) N);

    // print seed, N, number_of_workers, result, error
    printf("%d\t%d\t%d\t%f\t%f\n", seed, N, 1, my_pi, fabs(my_pi-M_PI));

    return EXIT_SUCCESS;

}

