! 
!  Copyright (C) 2015  CSC - IT Center for Science Ltd.
!
!  Licensed under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  Code is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  Copy of the GNU General Public License can be onbtained from
!  see <http://www.gnu.org/licenses/>.
! 

program thread_hello
  use omp_lib
  implicit none

  integer :: tid, nthreads

  !$omp parallel private(tid) shared(nthreads)
  !$omp single
  nthreads = omp_get_num_threads()
  print *, 'There are ', nthreads, ' threads in total.'
  !$omp end single

  tid = omp_get_thread_num()

  !$omp critical
  print *, 'Hello from thread id ', tid, '/', nthreads
  !$omp end critical
  !$omp end parallel
end program thread_hello
